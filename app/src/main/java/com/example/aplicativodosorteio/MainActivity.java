package com.example.aplicativodosorteio;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
      TextView textoResultado;
      EditText editTextMenor;
      EditText editTextMaior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editTextMenor =  (EditText) findViewById(R.id.editTextMenor);
        editTextMaior =  (EditText) findViewById(R.id.editTextMaior);
        textoResultado =  (TextView) findViewById(R.id.textoResultado);
    }

    public void sortearNumero( View view ){
        int intervalo = new Random().nextInt( Integer.parseInt
                (editTextMaior.getText().toString())-Integer.parseInt
                (editTextMenor.getText().toString())+Integer.parseInt
                (editTextMenor.getText().toString()));
        textoResultado.setText("O número selecionado é:"+ intervalo);

    }
}

